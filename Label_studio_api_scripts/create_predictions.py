# -*- coding: utf-8 -*-
""" Create a task with predictions on label studio.

Flow:
    1. import tasks
    2. create predictions
"""
import json
import requests

# The token for authorization
TOKEN_INFO = "Token 3430211161584df22667e0a6c8e91255bc630e2e"
LABEL_STUDIO = "http://localhost:6660"

def create_predictions(task_id=6):
    """ Create predictions of the task
    DOC:
        https://labelstud.io/api#operation/api_predictions_create
    API:
        POST, /api/predictions/
    """
    headers_info = {"Authorization": TOKEN_INFO, "Content-Type": "application/json"}
    target_url = 'http://localhost:6660/api/predictions'
    result_1 = {
        "value": { "start": 173, "end": 221, "text": ["Disease"]},
        "id": "0FlpscHBUV",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }

    result_2 = {
        "value": { "start": 197, "end": 209, "text": ["Disease"]},
        "id": "9VCEJZkYl5",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_3 = {
        "value": { "start": 211, "end": 222, "text": ["Disease"]},
        "id": "bY9rCDLDB6",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_4 = {
        "value": { "start": 224, "end": 241, "text": ["Disease"]},
        "id": "C6AdYimDgQ",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_5 = {
        "value": { "start": 243, "end": 251, "text": ["Disease"]},
        "id": "5BaHpYtpBE",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    '''result_6 = {
        "value": { "start": 735, "end": 747, "text": ["Disease"]},
        "id": "9-idRX5BsK",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_7 = {
        "value": { "start": 839, "end": 849, "text": ["Chemical"]},
        "id": "YCRu6-QSCn",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_8 = {
        "value": { "start": 675, "end": 686, "text": ["Chemical"]},
        "id": "UVb5wG-3Dn",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_9 = {
        "value": { "start": 907, "end": 917, "text": ["Disease"]},
        "id": "d9m1nB3N62",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_10 = {
        "value": { "start": 919, "end": 935, "text": ["Disease"]},
        "id": "8nDzzJpHMB",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_11 = {
        "value": { "start": 937, "end": 957, "text": ["Disease"]},
        "id": "60Hnj1r-Vv",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }
    result_12 = {
        "value": { "start": 962, "end": 979, "text": ["Disease"]},
        "id": "9hZo10ZOtX",
        "from_name": "label",
        "to_name": "text",
        "type": "labels",
        "origin": "manual"
    }'''
    payload = {
        "result": [result_1, result_2, result_3, result_4, result_5],
        "was_cancelled": "false",
        "ground_truth": "true",
        "lead_time": 0,
        "task": task_id
    }
    r = requests.post(target_url, headers=headers_info, data=json.dumps(payload))
    return r 


if __name__ == "__main__":
    rrr = create_predictions(task_id=6)
    print(rrr) # should be 201
    print(rrr.text)

