# -*- coding: utf-8 -*-
import json
import requests

# The token for authorization
TOKEN_INFO = "Token 3430211161584df22667e0a6c8e91255bc630e2e"
LABEL_STUDIO = "http://localhost:6660"

def import_tasks_via_url(project_id=1, file_url="/home/mintu/mnt/c/users/t05/downloads/yes/my_project/patient_discharge.txt"):
    """ Import tasks for a label-studio project.
    DOC:
        https://labelstud.io/api#operation/api_projects_import_create
    API:
        POST, /api/projects/{id}/import
    """
   # headers_info = {"Authorization": TOKEN_INFO}
   # target_url = 'http://localhost:6660/api/projects/{}/import'.format(project_id)
   # payload = {"files": file_url}
   # r = requests.post(target_url, headers=headers_info, data=payload)
    
    
    headers = {
    'Authorization': 'Token 3430211161584df22667e0a6c8e91255bc630e2e',
    }
    files = {
        'file': ('/mnt/c/Users/T05/Downloads/yes/my_project/patient_discharge.txt', open('/mnt/c/Users/T05/Downloads/yes/my_project/patient_discharge.txt', 'rb')),
    }
    response = requests.post('http://localhost:6660/api/projects/1/import', headers=headers, files=files)

    return response

if __name__ == "__main__":
    rrr = import_tasks_via_url(
        project_id=1,
        file_url="/home/mintu/mnt/c/users/t05/downloads/yes/my_project/patient_discharge.txt"
    )
    print(rrr) # should be 201
    print(rrr.status_code)
    print(rrr.json)
    print(rrr.text)
    if rrr.status_code != 201:
        raise ValueError("Status Code should be 201, but not".format(json))


