#!/bin/bash

docker run -it -d -p 6660:8080 \
  --name labelstudio_vs \
  -v `pwd`/mydata:/label-studio/data heartexlabs/label-studio:latest
