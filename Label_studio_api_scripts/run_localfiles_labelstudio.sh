#!/bin/bash

docker run -it -d -p 6660:8080 \
  --name labelstudio_vs1 \
  -e LABEL_STUDIO_LOCAL_FILES_SERVING_ENABLED=true \
  -e LABEL_STUDIO_LOCAL_FILES_DOCUMENT_ROOT=/label-studio/files \
  -v `pwd`/home/mintu/mnt/c/users/t05/downloads/yes/my_project:/label-studio/files \
  -v `pwd`/mydata:/label-studio/data heartexlabs/label-studio:latest
